package com.fiserv.action;

import java.util.List;

import org.json.JSONObject;

import com.fiserv.dao.EmployeeDao;
import com.fiserv.model.EmployeeModel;
import com.google.gson.Gson;
import com.opensymphony.xwork2.Action;

public class EmployeeAction implements Action{
	
	private String data;
	
	public String addEmployee(){
		Gson gson = new Gson();
		EmployeeModel employeeModel = gson.fromJson(data, EmployeeModel.class);
		if(employeeModel!=null){
			int status = EmployeeDao.saveOrUpdateEmployee(employeeModel);
		}
		List<EmployeeModel> list = EmployeeDao.getEmployeeList();
		data = gson.toJson(list);
		return Action.SUCCESS;
	}

	@Override
	public String execute() throws Exception {
		return null;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	
}
