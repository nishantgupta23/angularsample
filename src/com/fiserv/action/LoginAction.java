package com.fiserv.action;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.fiserv.dao.EmployeeDao;
import com.fiserv.model.EmployeeModel;
import com.google.gson.Gson;
import com.opensymphony.xwork2.Action;

public class LoginAction implements Action{
	
	private String data;

	public String login(){
		JSONObject json = new JSONObject(data);
		/*List<EmployeeModel> list = new ArrayList<>();
		EmployeeModel emp = new EmployeeModel();
		emp.setEmpid("1");emp.setEmpName("Nishant");
		emp.setEmpMobile("9998887770");emp.setEmpEmail("nishant@test.in");
		emp.setEmpAge(21);emp.setEmpSalary(1200);
		emp.setDepartment("EFT");emp.setEmpAddress("250, 4th Street, San Francisco, CA, 90003");
		emp.setEmpGender("Male");
		EmployeeModel emp1 = new EmployeeModel();
		emp1.setEmpid("2");emp1.setEmpName("Ethan");
		emp1.setEmpMobile("9998887770");emp1.setEmpEmail("ethan@test.in");
		emp1.setEmpAge(21);emp1.setEmpSalary(1200);
		emp1.setDepartment("EFT");emp1.setEmpAddress("250, 4th Street, San Francisco, CA, 90003");
		emp1.setEmpGender("Male");
		list.add(emp);list.add(emp1);*/
		List<EmployeeModel> list = EmployeeDao.getEmployeeList();
		Gson gson = new Gson();
		String finalJson = gson.toJson(list);
		//System.out.println(finalJson);
		data = gson.toJson(list);
		return Action.SUCCESS;
		
	}
	
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Override
	public String execute() throws Exception {
		return null;
	}
	
}
