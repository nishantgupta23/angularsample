package com.fiserv.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.fiserv.model.EmployeeModel;
import com.fiserv.util.Constants;
import com.mysql.jdbc.Statement;

public class EmployeeDao {

	/**
	 * Fetch all employees from database
	 * @return
	 */
	public static List<EmployeeModel> getEmployeeList(){
		List<EmployeeModel> list = new ArrayList<>();
		Connection connection = null;
		try {
			connection = DBConnection.getConnection();
			PreparedStatement statement = connection.prepareStatement(Constants.EMPLOYEE_LIST_SQL);
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()){
				EmployeeModel model = new EmployeeModel();
				model.setEmpid(resultSet.getString(1));
				model.setEmpName(resultSet.getString(2));
				model.setEmpMobile(resultSet.getString(3));
				model.setEmpEmail(resultSet.getString(4));
				model.setEmpAge(resultSet.getInt(5));
				model.setEmpAddress(resultSet.getString(6));
				model.setDepartment(resultSet.getString(7));
				model.setEmpSalary(resultSet.getFloat(8));
				model.setEmpGender(resultSet.getString(9));
				list.add(model);
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				DBConnection.closeConnection(connection);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return list;
	}
	
	/**
	 * Save or update an employee
	 * @param employeeModel
	 * @return
	 */
	public static int saveOrUpdateEmployee(EmployeeModel employeeModel) {
		int rows=0;
		Connection connection = null;
		try {
			connection = DBConnection.getConnection();
			PreparedStatement preparedStatement = null;
			if(employeeModel.getEmpid()!=null){
				preparedStatement = connection.prepareStatement(Constants.EMPLOYEE_UPDATE_SQL);
				preparedStatement.setString(9, employeeModel.getEmpid());
			}else{
				preparedStatement = connection.prepareStatement(Constants.EMPLOYEE_INSERT_SQL);
			}
			preparedStatement.setString(1, employeeModel.getEmpName());
			preparedStatement.setString(2, employeeModel.getEmpMobile());
			preparedStatement.setString(3, employeeModel.getEmpEmail());
			preparedStatement.setInt(4, employeeModel.getEmpAge());
			preparedStatement.setString(5, employeeModel.getEmpAddress());
			preparedStatement.setString(6, employeeModel.getDepartment());
			preparedStatement.setFloat(7, employeeModel.getEmpSalary());
			preparedStatement.setString(8, employeeModel.getEmpGender());
			
			boolean flag = preparedStatement.execute();
			System.out.println(flag);
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				DBConnection.closeConnection(connection);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rows;
	}
	
		
	public static void main(String[] aaa){
		EmployeeModel employeeModel = new EmployeeModel();
		employeeModel.setEmpName("Monika Sharma");
		employeeModel.setEmpMobile("8004557000");
		employeeModel.setEmpEmail("monika@aol.in");
		employeeModel.setEmpAge(22);
		employeeModel.setEmpAddress("23, test, test , India");
		employeeModel.setDepartment("HR");
		employeeModel.setEmpSalary(1400);
		employeeModel.setEmpGender("Female");
		employeeModel.setEmpid("1001");
		EmployeeDao.saveOrUpdateEmployee(employeeModel);
		for (EmployeeModel model : EmployeeDao.getEmployeeList()) {
			System.out.println(model.getEmpName());
		}
	}
}
