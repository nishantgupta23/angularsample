package com.fiserv.dao;

import static com.fiserv.util.Constants.DB_PASS;
import static com.fiserv.util.Constants.DB_USER;
import static com.fiserv.util.Constants.EMPLOYEE_DATA_SOURCE;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

	public static Connection getConnection() throws ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver");  
		Connection conn = DriverManager.getConnection(EMPLOYEE_DATA_SOURCE,DB_USER,DB_PASS);
		return conn;
	}
	
	public static void closeConnection(Connection connection) throws SQLException {		
		if (connection!=null) {
			connection.close();
		}
	}
	
	/*public static void main(String[] aaa){
		try {
			DBConnection.getConnection();
			System.out.println("Pass");
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
}
