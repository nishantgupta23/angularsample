package com.fiserv.util;

public class Constants {

	public static final String EMPLOYEE_DATA_SOURCE = "jdbc:mysql://localhost:3306/employee";
	public static final String DB_USER = "root";
	public static final String DB_PASS = "root";
	
	public static final String EMPLOYEE_CREATE_TABLE_SQL = "CREATE TABLE employee("+
															"emp_id INT(11) NOT NULL AUTO_INCREMENT, "+
															"emp_name VARCHAR(100) NOT NULL,"+
															"emp_mobile VARCHAR(40) NOT NULL,"+
															"emp_email VARCHAR(40) NOT NULL,"+
															"email_age INT(3) NULL DEFAULT NULL,"+
															"email_address VARCHAR(100) NULL DEFAULT NULL,"+
															"email_department VARCHAR(20) NULL DEFAULT NULL,"+
															"email_salary FLOAT NULL DEFAULT NULL,"+
															"gender VARCHAR(10) NULL DEFAULT NULL."+
															"PRIMARY KEY ('emp_id`) )";
	public static final String EMPLOYEE_LIST_SQL = "SELECT * FROM EMPLOYEE";
	public static final String EMPLOYEE_INSERT_SQL = "INSERT INTO EMPLOYEE (emp_name, emp_mobile, emp_email, email_age,"+
														"email_address, email_department, email_salary,gender) VALUES "+
														"(?,?,?,?,?,?,?,?);";
	public static final String EMPLOYEE_UPDATE_SQL = "UPDATE EMPLOYEE set emp_name=?,emp_mobile=?,"
														+ "emp_email=?,email_age=?, email_address=?, "
														+ "email_department=?, email_salary=?, gender=?"
														+"where emp_id=?";
}
