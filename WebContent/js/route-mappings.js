var angularStrutsApp = angular.module('EmployeeApp', ['EmployeeApp.controllers','ngRoute']);
angularStrutsApp.
config(['$routeProvider', function($routeProvider) {
	  $routeProvider.
		when("/drivers", {
			templateUrl: "html/drivers.html", 
			controller: "driversController"
		}).
		when("/employeeDetails/:id", {
			templateUrl: "html/updateEmployee.html", 
			controller: "viewController"
		}).
		when("/dashboard", {
			templateUrl: "html/dashboard.html",
			controller: "dashboardController"
		}).
		when("/home", {
			templateUrl: "html/login.html" 
		}).
		when("/addEmployee", {
			templateUrl: "html/addEmployee.html" 
		}).
		otherwise({
			redirectTo: '/home'
		});
	}]);