angular.module('EmployeeApp.services', [])
  .factory('employeeAPIservice', function($http) {

    var ergastAPI = {};
    var employeeList = {};
    var url = '/EmployeePortal';
    
    ergastAPI.getEmployees = function(json,actionURL){
    	var data = {data:json};
    	return $http({
    		method:'POST',
    		url:url+actionURL,
    		data:data,
    		headers: {
    			'Content-Type': 'application/json'
    		}
    	});
    }

    ergastAPI.setEmployeeList = function(list){
    	employeeList = list;
    }
    
    ergastAPI.getEmployeeList = function(){
    	return employeeList;
    }
    
    return ergastAPI;
  });