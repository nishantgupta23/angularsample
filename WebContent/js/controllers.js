angular.module('EmployeeApp.controllers', ['EmployeeApp.services']).

/* Login Controller*/
controller('loginController', ['$scope','employeeAPIservice','$location',function($scope, employeeAPIservice,$location) {
	$scope.model={};
	$scope.getEmployees = function(){
		var json = angular.toJson($scope.model);
		if($scope.form.user.$valid && $scope.form.pass.$valid){
			employeeAPIservice.getEmployees(json,'/loginAction').success(function (response) {
				//var list = [{"user":"nishant","pass":21},{"user":"Ankur","pass":22}];
				var list1 = angular.fromJson(response.data);
				employeeAPIservice.setEmployeeList(list1);
				$location.path('/dashboard');
			  });
		}
	};
	
}]).

/* Dashboard Controller*/
controller('dashboardController', ['$scope','employeeAPIservice',function($scope, employeeAPIservice) {
	$scope.employeeList = employeeAPIservice.getEmployeeList();	
}]).

/* Add Controller*/
controller('addController', ['$scope','employeeAPIservice','$location',function($scope, employeeAPIservice,$location) {
	$scope.model={};
	$scope.employeeList = employeeAPIservice.getEmployeeList();	
	
	$scope.addEmployee = function(){
		
		var json = angular.toJson($scope.model);
		if($scope.validate($scope.addForm)){
			employeeAPIservice.getEmployees(json,'/addEmployeeAction').success(function (response) {
				var list1 = angular.fromJson(response.data);
				employeeAPIservice.setEmployeeList(list1);
				$location.path('/dashboard');
			  }); 
		}
	};
	
	$scope.validate = function(form){
		if(form.empName.$valid &&
				form.empGender.$valid &&
				form.empMobile.$valid &&
				form.empEmail.$valid &&
				form.empAge.$valid &&
				form.department.$valid &&
				form.empSalary.$valid){
			return true;
		}else{
			return false;
		}
	};
}]).

/* Employee View Controller*/
controller('viewController', ['$scope','employeeAPIservice','$routeParams','$location',
                              function($scope, employeeAPIservice,$routeParams,$location) {
	$scope.employeeList = employeeAPIservice.getEmployeeList();	
	$scope.employee = $scope.employeeList[$routeParams.id];
	
	$scope.addEmployee = function(){
		var json = angular.toJson($scope.employee);
		if($scope.validate($scope.employeeForm)){
			employeeAPIservice.getEmployees(json,'/addEmployeeAction').success(function (response) {
				var list1 = angular.fromJson(response.data);
				employeeAPIservice.setEmployeeList(list1);
				$location.path('/dashboard');
			  }); 
		}
	};
	
	$scope.validate = function(form){
		if(form.empName.$valid &&
				form.empGender.$valid &&
				form.empMobile.$valid &&
				form.empEmail.$valid &&
				form.empAge.$valid &&
				form.department.$valid &&
				form.empSalary.$valid){
			return true;
		}else{
			return false;
		}
	};
}])

/*$scope.searchFilter = function (driver) {
      var re = new RegExp($scope.nameFilter, 'i');
      return !$scope.nameFilter || re.test(driver.Driver.givenName) || re.test(driver.Driver.familyName);
  };*/