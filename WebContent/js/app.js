angular.module('EmployeeApp', [
  'EmployeeApp.controllers',
  'EmployeeApp.services',
  'ngRoute'
]);